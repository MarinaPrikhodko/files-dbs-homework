package com.company;

public abstract class Testers extends Employee {

    Level level;

    public Testers(Level level) {
        this.level = level;
    }

    public abstract void test();

    @Override
    public void work() {
        System.out.println("Тестируем то, что придумали разработчики");

    }

    @Override
    public void rest() {
        System.out.println("Нашли время отдохнуть");

    }

    @Override
    public void eat() {
        System.out.println("Обеденный перерыв");

    }
}
