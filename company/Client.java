package com.company;

public class Client {

    public static void main(String[] args) {



        Developers junFrontDev=new FrontEndDevelopers(Level.JUNIOR);
        Developers midFrontDev=new FrontEndDevelopers(Level.MIDDLE);
        Developers senFrontDev=new FrontEndDevelopers(Level.SENIOR);
        Developers junBackDev= new BackEndDevelopers(Level.JUNIOR);
        Developers midBackDev= new BackEndDevelopers(Level.MIDDLE);
        Developers senBackDev= new BackEndDevelopers(Level.SENIOR);
        Testers junManTester=new ManualTesters(Level.JUNIOR);
        Testers midManTester=new ManualTesters(Level.MIDDLE);
        Testers senManTester=new ManualTesters(Level.SENIOR);
        Testers junATTester=new ATTesters(Level.JUNIOR);
        Testers midATTester=new ATTesters(Level.MIDDLE);
        Testers sinATTester=new ATTesters(Level.SENIOR);

        Employee[]employees=new Employee[12];
        employees[0]=junFrontDev;
        employees[1]=midFrontDev;
        employees[2]=senFrontDev;
        employees[3]=junBackDev;
        employees[4]=midBackDev;
        employees[5]=senBackDev;
        employees[6]=junManTester;
        employees[7]=midManTester;
        employees[8]=senManTester;
        employees[9]= junATTester;
        employees[10]=midATTester;
        employees[11]=sinATTester;

        for(Employee employee:employees){
            employee.eat();
            employee.work();
            employee.rest();

        }


    }
}
