package com.company;

public enum Level {
    JUNIOR,
    MIDDLE,
    SENIOR;
}
