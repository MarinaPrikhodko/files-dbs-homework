package com.company;

public abstract class Developers extends Employee {
    Level level;

    public Developers(Level level) {
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

    public abstract void develop();



    @Override
    public void work() {
        System.out.println("Разработчики придумывают что то новое");

    }

    @Override
    public void rest() {
        System.out.println("Устали придумывать, надо отдохнуть");

    }

    @Override
    public void eat() {
        System.out.println("Чтобы мозг лучше работал, надо подкрепиться");

    }
}
