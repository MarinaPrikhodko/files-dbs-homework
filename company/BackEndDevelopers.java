package com.company;

public class BackEndDevelopers extends Developers{

    public BackEndDevelopers(Level level) {
        super(level);
    }

    @Override
    public void develop() {
        System.out.println("Пишем код на Java");

    }
    @Override
    public void rest(){
        System.out.println("Делу время, а потехе час");
    }
}
