package com.company;

public abstract class Employee {
    String officeName = "Some Office";
    public abstract void work();
    public abstract void rest();
    public abstract void eat();

}
